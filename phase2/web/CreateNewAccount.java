/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.*;
import domain.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
//import dao.MemberJdbcDAO;

/**
 *
 * @author mirji507
 */
@WebServlet(name = "CreateNewAccount", urlPatterns = {"/CreateNewAccount"})
public class CreateNewAccount extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* Member details*/
            String firstName = request.getParameter("firstName");
            String lastName = request.getParameter("lastName");
            String address = request.getParameter("address");
            String postCode = request.getParameter("postCode");
            String phone = request.getParameter("phone");
            String email = request.getParameter("email");
            String[] interests = request.getParameterValues("involvement");
            String subscriptionType = request.getParameter("subscriptionType");
            
            /* Partner */
            String pFirstName = request.getParameter("partnerFirstName");
            String pLastName = request.getParameter("partnerLastName");
            String pAddress = request.getParameter("partnerAddress");
            String pCode = request.getParameter("partnerPostCode");
            String pPhone = request.getParameter("partnerPhone");
            String pEmail = request.getParameter("partnerEmail");
            String[] pInterest = request.getParameterValues("partnerInvolve");

            
            Member member = new Member();
            MemberJdbcDAO dao = new MemberJdbcDAO();
            //Generate id number
            int length = 7;
            Random random = new Random();
            char[] digits = new char[length];
            digits[0] = (char) (random.nextInt(9) + '1');
            for (int i = 1; i < length; i++) {
                
                digits[i] = (char) (random.nextInt(10) + '0');
            }
            
            member.setId(Integer.parseInt(new String(digits)));
            member.setFirstName(firstName);
            member.setLastName(lastName);
            member.setAddress(address + " " + postCode);
            member.setPhoneNumber(phone);
            member.setEmail(email);
            member.setSubscription(subscriptionType);
            member.setCatagories(Arrays.toString(interests));
            
            // Partner 
            //if(p.F){
            
            //}
            //System.out.println(member.toString() + " " + numChild + " " + numSibling);
            
            dao.save(member);
           
            response.sendRedirect("/info221-phase2/index.html");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
