drop table children;
drop table members;
drop table partner;
drop table sibling;

create table children(
ID integer not null,
FirstName varchar(20) not null,
lastName varchar(20) not null,
address varchar not null,
phoneNumber varchar(13) not null,
email varchar not null,
catagories varchar,
constraint pk_children primary key (ID)

);

create table partner(
ID integer not null,
FirstName varchar(20) not null,
lastName varchar(20) not null,
address varchar not null,
phoneNumber varchar(13) not null,
email varchar not null,
catagories varchar,
constraint pk_partner primary key (ID)

);
create table sibling(
ID integer not null,
FirstName varchar(20) not null,
lastName varchar(20) not null,
address varchar not null,
phoneNumber varchar(13) not null,
email varchar not null,
catagories varchar,
constraint pk_sibling primary key (ID)

);

create table members(
ID integer not null,
FirstName varchar(20) not null,
lastName varchar(20) not null,
address varchar not null,
phoneNumber varchar(13) not null,
email varchar not null,
partnersname varchar(20),
siblingsname varchar(20),
catagories varchar,
subscription varchar not null,
child varchar(20),
constraint pk_member primary key (ID)

);