/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import domain.Member;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author chike189
 */
public class MemberJdbcDAO implements MemberDAO {

    public void save(Member aMember) {
        String sql = "merge into members (id, firstName, lastName, address, phoneNumber, email, partnersName,"
                + "siblingsName, catagories, subscription, child ) values (?,?,?,?,?,?,?,?,?,?,?)";
        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters
            stmt.setInt(1, aMember.getId());
            stmt.setString(2, aMember.getFirstName());
            stmt.setString(3, aMember.getLastName());
            stmt.setString(4, aMember.getAddress());
            stmt.setString(5, aMember.getPhoneNumber());
            stmt.setString(6, aMember.getEmail());
            stmt.setString(7, aMember.getPartnersName());
            stmt.setString(8, aMember.getSiblingsName());
            stmt.setString(9, aMember.getCatagories().toString());
            stmt.setString(10, aMember.getSubscription());
            stmt.setString(11, aMember.getChild().toString());

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public void delete(Member aMember) {
        String sql = "delete from members where id = (?)";

        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters
            stmt.setInt(1, aMember.getId());

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    public void deleteChild() {
        String sql = "truncate table children";

        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    public void deleteSibling() {
        String sql = "truncate table sibling";

        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<Member> getAll() {
        String sql = "select * from members order by id";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();
// Create a collection for holding the student we get from the query.
// We are using a List in order to preserve the order in which
// the data was returned from the query.
            TreeSet<Member> members;
            members = new TreeSet<>();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");
                String partnersName = rs.getString("partnersName");
                String siblingsName = rs.getString("siblingsName");
                String catagories = rs.getString("catagories");
                String subscription = rs.getString("subscription");
// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, partnersName, siblingsName, catagories, subscription);
// and put it in the collection
                members.add(s);
            }
            return members;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<String> getCategories() {
        String sql = "select catagories from members order by catagories";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();
// Create a collection for holding the student we get from the query.
// We are using a List in order to preserve the order in which
// the data was returned from the query.
            Set catagories = new HashSet();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query

                String category = rs.getString("catagories");
// use the data to create a student object

// and put it in the collection
                catagories.add(category);
            }
            return catagories;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public Member getById(Integer aId) {
        String sql = "select * from members where id = (?);";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {

            stmt.setInt(1, aId);

// execute the query
            ResultSet rs = stmt.executeQuery();

            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");
                String partnersName = rs.getString("partnersName");
                String siblingsName = rs.getString("siblingsName");
                String catagories = rs.getString("catagories");
                String subscription = rs.getString("subscription");
// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, partnersName, siblingsName, catagories, subscription);
                return s;
            }
            return null;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<Member> getByCategories(String aCategory) {
        String sql = "select * from members where catagories = '" + aCategory + "' order by id;";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();

            Set products = new HashSet();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");
                String partnersName = rs.getString("partnersName");
                String siblingsName = rs.getString("siblingsName");
                String catagories = rs.getString("catagories");
                String subscription = rs.getString("subscription");
// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, partnersName, siblingsName, catagories, subscription);
// and put it in the collection
                products.add(s);
            }
            return products;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param aChild
     * @return
     */
    @Override
    public Collection<Member> getChild(String aChild) {
        String sql = "select * from members where child = '" + aChild + "' order by id;";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();

            Set products = new HashSet();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");
                String partnersName = rs.getString("partnersName");
                String siblingsName = rs.getString("siblingsName");
                String catagories = rs.getString("catagories");
                String subscription = rs.getString("subscription");
// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, partnersName, siblingsName, catagories, subscription);
// and put it in the collection
                products.add(s);
            }
            return products;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<Member> getChildren() {
        String sql = "select * from children;";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();

            Set products = new HashSet();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");

                String catagories = rs.getString("catagories");

// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, catagories);
// and put it in the collection
                products.add(s);
            }
            return products;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    public Collection<Member> getSibling() {
        String sql = "select * from sibling;";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();

            Set products = new HashSet();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");

                String catagories = rs.getString("catagories");

// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, catagories);
// and put it in the collection
                products.add(s);
            }
            return products;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    public void saveChildren(Member aMember) {
        String sql = "merge into children (id, firstName, lastName, address, phoneNumber, email, "
                + " catagories,  ) values (?,?,?,?,?,?,?)";
        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters
            stmt.setInt(1, aMember.getId());
            stmt.setString(2, aMember.getFirstName());
            stmt.setString(3, aMember.getLastName());
            stmt.setString(4, aMember.getAddress());
            stmt.setString(5, aMember.getPhoneNumber());
            stmt.setString(6, aMember.getEmail());

            stmt.setString(7, aMember.getCatagories().toString());

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    public void saveSibling(Member aMember) {
        String sql = "merge into sibling (id, firstName, lastName, address, phoneNumber, email, "
                + " catagories,  ) values (?,?,?,?,?,?,?)";
        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters
            stmt.setInt(1, aMember.getId());
            stmt.setString(2, aMember.getFirstName());
            stmt.setString(3, aMember.getLastName());
            stmt.setString(4, aMember.getAddress());
            stmt.setString(5, aMember.getPhoneNumber());
            stmt.setString(6, aMember.getEmail());

            stmt.setString(7, aMember.getCatagories().toString());

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    public void deletePartner() {
        String sql = "truncate table partner";

        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // we are forced to catch SQLException
            // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public void savePartner(Member aMember) {
        String sql = "merge into partner (id, firstName, lastName, address, phoneNumber, email, "
                + " catagories,  ) values (?,?,?,?,?,?,?)";
        try (
                // get connection to database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
            // copy the data from the student domain object into the SQL parameters
            stmt.setInt(1, aMember.getId());
            stmt.setString(2, aMember.getFirstName());
            stmt.setString(3, aMember.getLastName());
            stmt.setString(4, aMember.getAddress());
            stmt.setString(5, aMember.getPhoneNumber());
            stmt.setString(6, aMember.getEmail());

            stmt.setString(7, aMember.getCatagories().toString());

            // execute the statement
            stmt.executeUpdate();
        } catch (SQLException ex) { // don't let the SQLException leak from our DAO encapsulation
            throw new DAOException(ex.getMessage(), ex);
        }
    }

    @Override
    public Collection<Member> getPartner() {
        String sql = "select * from children;";
        try (
                // get a connection to the database
                Connection dbCon = ShoppingConnection.getConnection();
                // create the statement
                PreparedStatement stmt = dbCon.prepareStatement(sql);) {
// execute the query
            ResultSet rs = stmt.executeQuery();

            Set products = new HashSet();
            // iterate through the query results
            while (rs.next()) {
// get the data out of the query
                Integer id = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String address = rs.getString("address");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");

                String catagories = rs.getString("catagories");

// use the data to create a student object
                Member s = new Member(id, firstName, lastName, address, phoneNumber, email, catagories);
// and put it in the collection
                products.add(s);
            }
            return products;
        } catch (SQLException ex) {
            throw new DAOException(ex.getMessage(), ex);
        }
    }

}
